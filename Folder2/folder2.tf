provider "aws" {
  region = "us-east-2"
}
resource "aws_instance" "web2" {
  ami           = "ami-01e36b7901e884a10"
  instance_type = "t3.micro"

  tags = {
    Name = "Folder2"
  }
}

resource "aws_s3_bucket" "bucket2" {
  bucket = "my-tf-test-bucket2"

  tags = {
    Name = "Bucket2"
  }
}
