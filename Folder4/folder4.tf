provider "aws" {
  region = "us-east-2"
}
resource "aws_instance" "web4" {
  ami           = "ami-01e36b7901e884a10"
  instance_type = "t3.micro"

  tags = {
    Name = "Folder4"
  }
}

resource "aws_s3_bucket" "bucket4" {
  bucket = "my-tf-test-bucket4"

  tags = {
    Name = "Bucket4"
  }
}
