provider "aws" {
  region = "us-east-2"
}
resource "aws_instance" "web3" {
  ami           = "ami-01e36b7901e884a10"
  instance_type = "t3.micro"

  tags = {
    Name = "Folder3"
  }
}

resource "aws_s3_bucket" "bucket3" {
  bucket = "my-tf-test-bucket3"

  tags = {
    Name = "Bucket3"
  }
}
