provider "aws" {
  region = "us-east-2"
}
resource "aws_instance" "web6" {
  ami           = "ami-01e36b7901e884a10"
  instance_type = "t3.micro"

  tags = {
    Name = "Folder6"
  }
}

resource "aws_s3_bucket" "bucket6" {
  bucket = "my-tf-test-bucket6"

  tags = {
    Name = "Bucket6"
  }
}
