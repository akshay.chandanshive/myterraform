provider "aws" {
  region = "us-east-2"
}
resource "aws_instance" "web5" {
  ami           = "ami-01e36b7901e884a10"
  instance_type = "t3.micro"

  tags = {
    Name = "Folder5"
  }
}

resource "aws_s3_bucket" "bucket5" {
  bucket = "my-tf-test-bucket5"

  tags = {
    Name = "Bucket5"
  }
}
