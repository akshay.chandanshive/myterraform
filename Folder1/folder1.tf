provider "aws" {
  region = "us-east-2"
}
resource "aws_instance" "web1" {
  ami           = "ami-01e36b7901e884a10"
  instance_type = "t3.micro"

  tags = {
    Name = "Folder1"
  }
}

resource "aws_s3_bucket" "bucket1" {
  bucket = "my-tf-test-bucket1"

  tags = {
    Name = "Bucket1"
  }
}
